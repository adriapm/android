package com.example.adriapm.helloworldidiomes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
private Switch switch1;
private TextView text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        switch1 = (Switch) findViewById(R.id.switch1);
        switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                     text = findViewById(R.id.editText);
                    text.setBackgroundColor(getResources().getColor(R.color.negre));
                    text.setTextColor(getResources().getColor(R.color.blanc));
                }else {
                     text = findViewById(R.id.editText);
                    text.setBackgroundColor(getResources().getColor(R.color.blanc));
                    text.setTextColor(getResources().getColor(R.color.negre));
                }
            }
        });
    }
    public void  suma(View v){
       text = findViewById(R.id.editText);
        text.setText(String.valueOf(Integer.parseInt(text.getText().toString())+1));

    }
    public void  resta(View v){

        text = findViewById(R.id.editText);

                text.setText(String.valueOf(Integer.parseInt(text.getText().toString())-1));

    }
    public void  aug(View v){
       text = findViewById(R.id.editText);
        float textSize =text.getTextSize()+10;
        text.setTextSize(TypedValue.COMPLEX_UNIT_PX,textSize);
    }
    public void  dis(View v){
        text = findViewById(R.id.editText);
        float textSize =text.getTextSize()-10;
        text.setTextSize(TypedValue.COMPLEX_UNIT_PX,textSize);

    }
    public void  mostrar(View v){
        text = findViewById(R.id.editText);
        text.setEnabled(true);

    }
    public void  amagar(View v){
        text = findViewById(R.id.editText);
        text.setEnabled(false);
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putFloat("textSize", text.getTextSize());
        outState.putInt("textColor", text.getCurrentTextColor());
        outState.putCharSequence("textValue", text.getText());
    }
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        text.setTextSize(savedInstanceState.getFloat("textSize"));
        text.setTextColor(savedInstanceState.getInt("textColor"));
        text.setText(savedInstanceState.getCharSequence("textValue"));

    }


}
