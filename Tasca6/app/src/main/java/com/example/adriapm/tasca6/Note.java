package com.example.adriapm.tasca6;

/**
 * Created by adriapm on 09/02/18.
 */

public class Note {
    private String mTitle;
    private String mDescription;
    private boolean mImportant;

    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public boolean ismImportant() {
        return mImportant;
    }

    public void setmImportant(boolean mImportant) {
        this.mImportant = mImportant;
    }
}
