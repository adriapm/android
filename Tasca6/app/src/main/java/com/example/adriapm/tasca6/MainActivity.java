package com.example.adriapm.tasca6;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private NoteAdapter mNoteAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mNoteAdapter = new NoteAdapter();
        ListView listNote = (ListView) findViewById(R.id.listView);
        listNote.setAdapter(mNoteAdapter);

        listNote.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int whichItem, long id) {
                // TODO Create a temporary Note which is a reference to the Note
                // that has just been clicked
                // TODO Create a new dialog window
                // TODO Send in a reference to the note to be shown
                // TODO Show the dialog window with the note in it
            }
            });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){

        getMenuInflater().inflate(R.menu.menu_principal, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        if (item.getItemId() == R.id.item1) {
            DialogNewNote dialog = new DialogNewNote();
            dialog.show(getFragmentManager(), "");
            return true;
        }
        return false;
    }

    public class NoteAdapter extends BaseAdapter {
        List<Note> noteList = new ArrayList<Note>();
        @Override
        public int getCount() {

            return noteList.size();
        }

        @Override
        public Note getItem(int whichItem) {
            return noteList.get(whichItem);
        }
        @Override
        public long getItemId(int whichItem) {
            return whichItem;
        }
        @Override
        public View getView(int whichItem, View view, ViewGroup viewGroup)
        {
// Has view been inflated already
            if(view == null){
// If not, do so here
// First create a LayoutInflater
                LayoutInflater inflater = (LayoutInflater)
                        getSystemService(Context.LAYOUT_INFLATER_SERVICE);
// Now instantiate view using inflater.inflate
// using the listitem layout
                view = inflater.inflate(R.layout.layout_list_item, viewGroup,false);
// The false parameter is neccessary
// because of the way that we want to use listitem
            }
// Grab a reference to all our TextView and ImageView widgets
            TextView txtTitle = (TextView) view.findViewById(R.id.txtTitleItem);
            TextView txtDescription = (TextView) view.findViewById(R.id.txtDescriptionItem);
            ImageView ivImportant = (ImageView) view.findViewById(R.id.imageViewImportantItem);
// Hide any ImageView widgets that are not relevant
            Note tempNote = noteList.get(whichItem);
            if (!tempNote.ismImportant()){
                ivImportant.setVisibility(View.GONE);
            }
// Add the text to the heading and description
            txtTitle.setText(tempNote.getmTitle());
            txtDescription.setText(tempNote.getmDescription());
            return view;}
        // This class is not overriden. We are the owner.
        public void addNote(Note n){
            noteList.add(n);
            notifyDataSetChanged();

            SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(n.getmTitle(),"titol");
            editor.putString(n.getmDescription(),"desc");
            editor.putBoolean("important", n.ismImportant());
        }

    }

    public void createNewNote(Note n){
        mNoteAdapter.addNote(n);
    }

}
