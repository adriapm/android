package com.example.adriapm.tasca6;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

/**
 * Created by adriapm on 09/02/18.
 */
public class DialogNewNote extends DialogFragment {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View newNote = inflater.inflate(R.layout.dialog_new_note, null);
        final EditText editTitle = newNote.findViewById(R.id.editTitle);
        final EditText editDescription = newNote.findViewById(R.id.editDescription);final CheckBox checkBoxImportant =
                newNote.findViewById(R.id.checkBoxImportant);
        Button btnCancel = (Button) newNote.findViewById(R.id.btnCancel);
        Button btnOK = (Button) newNote.findViewById(R.id.btnCreate);
        builder.setView(newNote).setMessage("Afegeix una nova anotació");
// Handle the cancel button
        btnCancel.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
// Handle the Create button
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
// Create a new note
                Note newNote = new Note();
// Set its variables to match the users entries on the form
                newNote.setmTitle(editTitle.getText().toString());
                newNote.setmDescription(editDescription.getText().toString());
                newNote.setmImportant(checkBoxImportant.isChecked());
// Pass newNote back to MainActivity
                MainActivity main = (MainActivity) getActivity();
                main.createNewNote(newNote);
// Quit the dialog
                dismiss();
            }
        });
        return builder.create();
    }
}