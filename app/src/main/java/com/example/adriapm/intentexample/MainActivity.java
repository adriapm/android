package com.example.adriapm.intentexample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void intentEscollir(View view) {
        Intent intent = new Intent(this, intentEscull.class);
        startActivityForResult(intent, 1);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        int a  = data.getIntExtra("Resultat", '0');
        String[] h = getResources().getStringArray(R.array.spinnerItems);
        EditText hola = findViewById(R.id.editText2);
        hola.setText(h[a]);
    }


}
