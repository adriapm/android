package com.example.adriapm.tasca5;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


public class MainActivity extends AppCompatActivity {

    FragmentImatge fragmentImatge;
    FragmentLlista fragmentLlista;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragmentImatge  = new FragmentImatge();
        fragmentLlista  = new FragmentLlista();
        getSupportFragmentManager().beginTransaction().add(R.id.frameLayout, fragmentImatge).commit();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){

        getMenuInflater().inflate(R.menu.menuprincipal, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case R.id.item1:
                getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, fragmentImatge).commit();
                return true;
            case R.id.item2:
                getSupportFragmentManager().beginTransaction().replace(R.id.frameLayout, fragmentLlista).commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

}
